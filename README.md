# OverSight Internal Protocol (OsProto)

![alt text](assets/logo.jpg)

### Changing your perception of reality by leveraging the power of mixed reality.

- [www.oversight-ar.com][oversight] 

### Prerequisites

- Install [Docker][docker]

### Instructions ###
Run scripts/generate-protocol.cmd {output directory}

[oversight]: https://oversight-ar.com/
[docker]: https://docs.docker.com/get-docker/