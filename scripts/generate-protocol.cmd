@echo off

set output_folder=/mnt/csharp_out
set /p target=Please choose generation target (client only - 1, server only - 2, both - 3):

if %target%==1 goto client_only
if %target%==2 goto server_only
if %target%==3 goto both

echo "unsupported input: %target%"
goto end

:generate_files
cd ..
mkdir csharp_out
docker run --rm -v %cd%:/mnt uber/prototool:latest protoc --plugin=protoc-gen-grpc=/usr/bin/grpc_csharp_plugin --csharp_out=%output_folder% --grpc_out=%grpc_out% -I /mnt /mnt/common.proto
docker run --rm -v %cd%:/mnt uber/prototool:latest protoc --plugin=protoc-gen-grpc=/usr/bin/grpc_csharp_plugin --csharp_out=%output_folder% --grpc_out=%grpc_out% -I /mnt /mnt/rotem.proto
cd scripts
goto end

:server_only
echo "Compiling server RPC & messages files"
set grpc_out=no_client:%output_folder%
goto generate_files

:client_only
echo "Compiling client RPC & messages files"
set grpc_out=no_server:%output_folder%
goto generate_files

:both
echo "Compiling client & server RPC & messages files"
set grpc_out=%output_folder%
goto generate_files

:end
pause